---
author: Shank
categories:
- git
- git annex
- quicknotes
date: 2015-06-23T17:52:44+05:30
description: ""
draft: false
slug: quicknotes-git-annex-2
tags:
- git
- git annex
- quicknotes
title: Quicknotes - Git Annex
---

*Before using **git annex***, you need to declare that this is a git annex repository

```shell
git annex init
```

***Add a file*** to git annex

```shell
git annex add <filename>
```

To ***add the content to the server***, just adding the file to git annex is not sufficient. You need to do
```shell
git annex sync --content
```

When you clone or pull a repo, only the annex symlinks are downloaded, the files aren't downloaded. To download the annexed file,

```shell
git annex get <filename>
```

`git annex sync --content` also downloads the annexed files
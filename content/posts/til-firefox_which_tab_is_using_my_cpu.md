---
author: Shank
title: "Today I Learned : Firefox - Which tab is using my CPU?"
date: 2019-02-08T19:56:29+05:30
draft: false
categories:
- today-i-learned
- firefox
tags:
- today-i-learned
- firefox
slug: til-firefox_which_tab_is_using_my_cpu
---

Navigate to **`about:performance`** url. This will bring up the **_task manager_** for Firefox.

Firefox Task Manager displays the following information:

- Name of the item
- Type (tab/addon)
- Energy Impact (roughly equivalent to load on CPU)
- Memory Impact

Here is a screenshot showing this page.

![Firefox Task Manager](/images/2019/til_firefox_task_manager/firefox_task_manager.png)
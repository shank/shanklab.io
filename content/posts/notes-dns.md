---
title: Notes - DNS (Domain Name System)
slug: notes-dns
date: 2018-06-21T16:59:17+05:30
draft: false
tags: ["notes", "computer-science"]
---

![Notes - DNS -1](/images/2018/notes_dns/notes_dns_1.jpg)
![Notes - DNS -2](/images/2018/notes_dns/notes_dns_2.jpg)
![Notes - DNS -3](/images/2018/notes_dns/notes_dns_3.jpg)

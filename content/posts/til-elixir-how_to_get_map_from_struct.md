---
author: Shank
categories:
- elixir
- today-i-learned
date: 2017-09-22T20:49:19+05:30
description: ""
draft: false
slug: til-elixir-how_to_get_map_from_struct
tags:
- elixir
- today-i-learned
title: 'Today I Learned : Elixir - How to get a map from a struct'
---

Use the [`Map.from_struct`](https://hexdocs.pm/elixir/Map.html#from_struct/1) function.

```elixir
struct = %User{first_name: "Jose", last_name: "Valim"}
map = Map.form_struct(struct)
```
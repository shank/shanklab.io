+++
title = "Today I Learned : Arrows keys in MIT-Scheme interpreter"
slug = "til-arrows_keys_in_mit_scheme_interpreter"
date = "2019-02-03T11:46:59+05:30"
draft = false
tags = ["today-i-learned", "sicp", "scheme"]
+++

The [mit-scheme](https://www.gnu.org/software/mit-scheme/) interpreter does not bring the previous/next command when we press the up/down arrow keys. To add this behaviour, use `rlwrap` (which wraps around [Readline](https://en.wikipedia.org/wiki/GNU_Readline)).

*Installation:*

- OSX: `brew install rlwrap`
- Linux: `sudo apt install rlwrap`
- Windows: ¯\\\_(ツ)\_/¯

Afterwards, launch the `mit-scheme` interpreter as **`rlwrap scheme`**.

Source: [StackOverflow](https://stackoverflow.com/questions/31768839/how-do-i-make-arrow-keys-work-in-mit-scheme-interpreter)
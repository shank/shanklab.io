---
title: "Today I Learned : Curl - Follow Redirects"
slug: til-curl-follow_redirects
date: 2018-06-20T01:40:41+05:30
tags: ["today-i-learned", "curl"]
draft: false
---

Use the `-L` flag as below.

```shell
curl -L google.com
```
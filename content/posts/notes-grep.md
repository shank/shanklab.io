---
author: Shank
categories:
- utils
- '*nix'
- grep
date: 2015-09-17T15:03:34+05:30
description: ""
draft: false
slug: notes-grep
tags:
- utils
- '*nix'
- grep
title: Notes - Grep
---

![Grep Notes](/images/2015/09/grep-3.png)

Source: Notes are from the [video tutorial](https://www.youtube.com/watch?v=QEpip5Ja1ac) by Frank Perez

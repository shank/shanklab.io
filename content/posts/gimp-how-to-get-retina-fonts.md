---
author: Shank
categories:
- gimp
- mac
- retina
date: 2015-09-17T05:32:42+05:30
description: ""
draft: false
slug: gimp-how-to-get-retina-fonts
tags:
- gimp
- mac
- retina
title: GIMP - How to get retina fonts
---

1. Right-click the gimp.app and choose “Show Package Content”.
2. Edit the file info.plist
3. Just above:  
```xml
    </dict>
    </plist>
```
Place this:
```xml
    <key>NSHighResolutionCapable</key>
    <true/>
```
4. Now in order for the OSX to update the change, make a copy of the gimp.app, remove the old and rename.
5. GIMP fonts would not be in retina resolution. However the icons would still be blurry and need a different fix.

**Source**: [jayway.com](http://www.jayway.com/2012/11/11/have-you-just-purchased-a-new-macbook-pro-with-a-retina-display-here-is-a-guide-for-getting-non-retina-ready-applications-to-display-the-correct-resolution/)  
---
author: Shank
categories:
- vagrant
date: 2015-09-13T20:36:11+05:30
description: ""
draft: false
slug: vagrant-up-and-running
tags:
- vagrant
title: '[WIP] Notes : Vagrant'
---

Some useful aliases (add to ~/.zshrc) -
```bash
vdestroy='vagrant destroy'
vht='vagrant halt'
vit='vagrant init'
vsd='vagrant suspend'
vssh='vagrant ssh'
vst='vagrant status'
vup='vagrant up'
```
Installation -
```shell
brew install vagrant
```

Installing plugins -
```shell
vagrant plugin install <plugin_name>
```

**Plugins** that I use [ //TODO ] -  
- `vagrant-proxyconf` >> for using behind proxy  
- `vagrant-vbguest` >> for automatically installing host's virtualbox guest additions in the guest os

**Adding a Box** (downloads it so that it can be used further)
```shell
vagrant box add debian/jessie64
```

Init (creates a vagrantfile in the current directory)
```shell
vagrant init debian/jessie64
```

**Destroy** the current vagrant machine
```shell
vagrant destroy
```

> Always **place** the `Vagrantfile` under version conntrol

> **Dont** place the `.vagrant` folder in version control

Add **Synced folder**
```ruby
config.vm.synced_folder "../data", "/vagrant_data"
```
The shared filesystem gives you a place to store files that won't be destroyed as a part of `vagrant destroy`.

Shared filesystems just show up as normal folders on the host, and are therefore easily backed up.

Shared folders incur heavy performance penalty within the virtual machine when there is heavy I/O, so they should only be used for source files.

**Forward Port** from host to guest
```ruby
config.vm.network "forwarded_port", guest:80, host:8080
```
**Provisioning** - problem of installing software on a booted system

> Vagrant, by default, runs all the provisioning shell scripts as root so there is no need to include `sudo` in the commands specified in the `provision.sh` file

The provisioning method that I commonly use -
```ruby
# run the provisioning commands that need root
config.vm.provision "shell", privilege: true, path: "provision_root.sh"

# non-root provisioning commands
config.vm.provision "shell", privilege: false, path: "provision_user.sh"
```
+++
title = "Today I Learned : Elixir - How to IEx.pry() in mix tests"
date = "2019-02-25T10:34:37+05:30"
draft = false
categories = [ "today-i-learned", "elixir" ]
tags = [ "today-i-learned", "elixir" ]
slug = "til-elixir_how_to_iex_pry_in_mix_tests"
+++

To use `IEx.pry` the `mix` tests have to be run via `iex` using

```shell
iex -S mix test
```
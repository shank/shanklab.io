+++
title = "Advent of Code 2020 in Rust - Day 1 Learnings"
slug = "aoc2020-rust_day1_learnings"
date = "2020-12-02T13:57:00+05:30"
draft = false
tags = ["advent-of-code", "rust"]
categories = ["advent-of-code", "rust"]
+++

I shall be doing the [Advent of Code 2020](https://adventofcode.com/2020) using [Rust](https://rust-lang.org/) and shall be noting down any and all tips, snipppets etc. that I had to look up and the lessons learned, just as a resource for my future self.

The solutions can be found at [gitlab](https://gitlab.com/shank/advent-of-code).

Stuff that I had to look up or that I learned working on [day 1 problem](https://adventofcode.com/2020/day/1):

* [get name of current executable](#get-name-of-current-executable)
* [list entries in a directory](#list-entries-in-a-directory)
* When you dont care about the specifics of the error, its sometimes convinient to [convert `Result` to `Option`](#convert-`result`-to-`option`)
* [read lines from a file (and the `bufreader`)](#read-lines-from-a-file-(and-the-bufreader))

---

## Get name of current executable

I decided to save each puzzle's input in text files named `day<x>_1.in` and `day<x>_2.in`. So for first puzzle, inputs will be in `day1_1.in` and `day1_2.in` respectively. Also I decided to <TODO>

The [`std::env::current_exe()`](https://doc.rust-lang.org/std/env/fn.current_exe.html) function returns the path of the current running executable.

Wrapping it in a nice resusable function (source: [programming-idioms.com](https://www.programming-idioms.org/idiom/105/current-executable-name/1887/rust)):

```rust
fn get_exec_name() -> Option<String> {
    std::env::current_exe()
        .ok()
        .and_then(|pb| pb.file_name().map(|s| s.to_os_string()))
        .and_then(|s| s.into_string().ok())
}
```

## List entries in a directory

TODO

## Convert `Result` to `Option`

TODO

## Reading lines form a file (and the `bufreader`)

TODO

---
author: Shank
categories:
- python
- pip
- pip3
date: 2015-09-14T02:55:54+05:30
description: ""
draft: false
slug: assertionerror-on--pip-list-
tags:
- python
- pip
- pip3
title: AssertionError on `pip list`
---

I am using `pip3`, but all the following commands have `pip` equivalent too.

When doing a `pip3 list`, I was getting the following output:
```shell
argparse (1.4.0)
chardet (2.3.0)
cloud-init (0.7.7)
colorama (0.3.2)
command-not-found (0.3)
configobj (5.0.6)
html5lib (0.999)
Jinja2 (2.7.3)
jsonpatch (1.3)
jsonpointer (1.0)
language-selector (0.1)
MarkupSafe (0.23)
oauthlib (0.6.1)
pbr (1.7.0)
pip (1.5.6)
prettytable (0.7.2)
pycrypto (2.6.1)
pycurl (7.19.5)
pygobject (3.14.0)
pyserial (2.6)
Exception:
Traceback (most recent call last):
  File "/usr/lib/python3/dist-packages/pip/basecommand.py", line 122, in main
    status = self.run(options, args)
  File "/usr/lib/python3/dist-packages/pip/commands/list.py", line 80, in run
    self.run_listing(options)
  File "/usr/lib/python3/dist-packages/pip/commands/list.py", line 142, in run_listing
    self.output_package_listing(installed_packages)
  File "/usr/lib/python3/dist-packages/pip/commands/list.py", line 151, in output_package_listing
    if dist_is_editable(dist):
  File "/usr/lib/python3/dist-packages/pip/util.py", line 367, in dist_is_editable
    req = FrozenRequirement.from_dist(dist, [])
  File "/usr/lib/python3/dist-packages/pip/__init__.py", line 299, in from_dist
    assert len(specs) == 1 and specs[0][0] == '=='
AssertionError
```
To resolve this try the following options:
#### Option 1
```shell
pip3 install -U distribute
```
However this did not fix it for me. What worked for me was **Option 2**

#### Option 2
```shell
# remove the ubuntu package
sudo apt-get remove --auto-remove pip3

# download the official pip installer
wget https://bootstrap.pypa.io/get-pip.py

# install pip
sudo python3 get-pip.py

# add sym-link to make it run without full path: 
sudo ln -s /usr/local/bin/pip /usr/bin/pip
```
---
author: Shank
categories:
- linux
- utils
- mac
- '*nix'
- tmux
date: 2015-09-23T14:10:48+05:30
description: ""
draft: false
slug: tmux-cheatsheet
tags:
- linux
- utils
- mac
- '*nix'
- tmux
title: tmux - cheatsheet
---

![tmux commands](/images/2015/09/tmux_commands.png)

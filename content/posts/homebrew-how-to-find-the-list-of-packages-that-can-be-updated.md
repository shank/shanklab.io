---
author: Shank
categories:
- mac
- brew
- homebrew
date: 2015-09-17T16:31:01+05:30
description: ""
draft: false
slug: homebrew-how-to-find-the-list-of-packages-that-can-be-updated
tags:
- mac
- brew
- homebrew
title: Homebrew - how to find the list of packages that can be updated
---

The command to find the list of packages that can be updated, is not listed on the help page of `brew`.

The command is:

```shell
brew outdated
```
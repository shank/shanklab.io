---
author: Shank
categories:
- sql
- database
- postgresql
- work-in-progress
date: 2015-07-30T06:45:27+05:30
description: ""
draft: true
slug: wip-postgresql-notes
tags:
- sql
- database
- postgresql
- work-in-progress
title: '[WIP] PostgreSQL Notes'
---

**PostgreSQL Database Objects**
- `service`
- `database`
- `schema`
- `catalog`
- `variable`
- `extension`
- `table`
- `foreign table` and `foreign data wrapper`
- `tablespace`
- `view`
- `function`
- `language`
- `operator`
- `type`
- `cast`
- `sequence`
- `row` or `record`
- `trigger`
- `rule`

**Main Configuration Files**
- *postgres.conf*
- *pg_hb.conf*
- *pg_ident.conf*

An easy way to check the *current settings* is to query the `pg_settings` view

Restarting terminates active connections, whereas reloading does not.

Authentication Methods
- `trust`
- `md5`
- `password`
- `ident`
- `peer`

**init** a database directory
```shell
pg_ctl initdb -D <data_dir>
```

**start** a postgres server
```shell
pg_ctl start -D <data_dir>
```

**stop** a postgres server
```shell
pg_ctl stop -m fast -D <data_dir>
```

**restart** a postgres server
```shell
pg_ctl restart -m fast -D <data_dir>
```

**reload the conf** file from inside psql
```sql
select pg_reload_conf();
```

To reload configuration changes from command line
```shell
pg_ctl reload -D <data_dir>
```

Retieve a list of recent connections and process ids
```sql
select * from pg_stat_activity;
```

Cancel all active queries on a connection
```sql
select pg_cancel_backend(procid)
```

Kill the connection
```sql
select pg_terminate_backend(procid)
```

**Roles**
- login roles
- group roles
- group login roles

**create login role**
```sql
create role leo login password 'king' createdb valid until 'infinity';
```

**create superuser role**
```sql
create role regina login password 'queen' superuser valid until '2020-1-1 00:00';
```

**create a group role**
```sql
create role royalty inherit;
```

**add roles to a group role**
```sql
grant royalty to leo;
grant royalty to regina;
```

**Create a database** modelled after a template
```sql
create database my_db template my_template_db;
```

**Schemas** organise your database into logical groups. A common way to organise schemas is by roles

to find out the current loggin in user
```sql
select user;
```

As a best-practice, *install extensions into a seperate schema*

WITH GRANT OPTION - the grantee can grant onwards
```sql
grant all on all tables in schema public to mydb_admin with grant option;
```

Revoke privileges
```sql
revoke execute on all functions in schema my_schema from public;
```

People often forget to set GRANT USAGE ON SCHEMA or GRANT ALL ON SCHEMA. Even if your tables and functions have rights assigned to a role, these tables and functions will still not be accessible if the role has not USAGE rights to the schema.

To view all extension binaries already available on your server
```sql
select * from pg_available_extensions;
```

Remove an extension
```sql
drop extension
```

Install and extension
```sql
create extension <extn_name> schema my_extensions;
```

`pg_dump` and `pg_dumpall`


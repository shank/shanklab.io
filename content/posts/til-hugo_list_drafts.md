---
author: Shank
title: "Today I Learned: Hugo - List posts that are in draft mode"
date: "2019-02-12T19:40:20+05:30"
draft: false
categories:
- today-i-learned
- hugo
tags:
- today-i-learned
- hugo
slug: til-hugo_list_drafts
---

Simply navigate to the root folder for the project and do:

```shell
hugo list drafts
```
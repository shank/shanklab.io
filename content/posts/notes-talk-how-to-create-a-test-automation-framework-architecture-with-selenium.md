---
author: Shank
categories:
- selenium
- automation testing
- testing
date: 2015-07-03T18:29:53+05:30
description: ""
draft: false
slug: notes-talk-how-to-create-a-test-automation-framework-architecture-with-selenium
tags:
- selenium
- automation testing
- testing
title: Notes - Talk - How To Create a Test Automation Framework Architecture with
  Selenium
---

### [Talk](https://www.youtube.com/watch?v=DO8KVe00kcU) by John Sonmez

**Types of Automated Testing**

- Unit Testing
- Integration Testing
- Blackbox Automated Testing (System Test, UAT, AT etc.)

**Unit Testing** >> tests the smallest unit of code in isolation (say a class)

**Integration Testing** >> tests units together, but still focuses on the code

**Black-box Automated Testing** >> test the system from the perspective of the user

**Why !**

- Manual Testing >> Regression !
- Absolute Requirements
- BATs test much more production code with fewer lines of BAT code

**Common Failure Points**

- Recorded, Brittle Tests >> Never works !
- Not building a framework
- Writing tests like code

**Steps to take**

- Separate Automation Framework
- Simple Tests
- Tests Drive development of the framework

**Architecture**

Tests >> Framework >> Selenium >> Browser (Web App)

**The Page Pattern**

- Pages are a good way to model the functionality of an application
- Every Route represents a page as far as our tests are concerned
- write tests for functions from the user's perspective
- Ease of use and cleaner code

**Think About Layers**
Workflows >> Pages >> Navigation / UI Utilities >> Selenium

**Some Basic Rules (NEVER)**

- NEVER require tests to declare variables
- NEVER require test to use the "new" keyword or create new objects
- NEVER require the tests to manage state on their own
- NEVER expose the browser or DOM to the tests or let them manipulate it directly

**Some Basic Rules (ALWAYS)**

- ALWAYS reduce the number of parameter for API calls when possible
- ALWAYS use default values instead of requiring parameters when possible

**Some Basic Rules (PREFER)**

- PREFER to make the API easier to use over making the internals of the API less complex
- PREFER using enumerations and constants to requiring the test to pass in primitive types

> If tests are complex they wont be maintained

Internal **DSL** >> that's more readable and usable

**Errors v/s Failures** >> Test should fail only when what they test has failed !

Use **Continuous Integration** >> makes automated tests part of your builds
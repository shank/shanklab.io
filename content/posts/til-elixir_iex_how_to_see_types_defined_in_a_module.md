+++
title = "Today I Learned - Elixir - How to see the types defined by a module in IEx"
slug = "til-elixir_iex_how_to_see_types_defined_in_a_module"
date = "2019-02-28T08:30:00+05:30"
draft = false
tags = ["today-i-learned", "elixir"]
categories = ["today-i-learned", "elixir"]
+++

You can display typespecs from a module thorugh `t` command on iex, i.e:

```shell
iex(1)> t Enum
@type t() :: Enumerable.t()
@type acc() :: any()
@type element() :: any()
@type index() :: integer()
@type default() :: any()
```

Reference: [IEx.Helpers.t](https://hexdocs.pm/iex/1.8.0/IEx.Helpers.html#t/1)
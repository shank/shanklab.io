---
author: Shank
categories:
- git
date: 2015-09-28T08:50:57+05:30
description: ""
draft: false
slug: git-commit-message-tips
tags:
- git
title: Git commit message - tips
---

The seven rules of a great git commit message

- Separate subject from body with a blank line
- Limit the subject line to 50 characters
- Capitalize the subject line
- Do not end the subject line with a period
- Use the imperative mood in the subject line
- Wrap the body at 72 characters
- Use the body to explain what and why vs. how

**Source**: [Chris Beams Blog](http://chris.beams.io/posts/git-commit/)
---
author: Shank
categories:
- python
- sqlalchemy
date: 2015-06-30T09:13:42+05:30
description: ""
draft: false
slug: sqlalchemy-get_one_or_create
tags:
- python
- sqlalchemy
title: SQLAlchemy - get_one_or_create()
---

DISCLAIMER: This code is taken from [this](http://skien.cc/blog/2014/01/15/sqlalchemy-and-race-conditions-implementing/) blog post  

```python
def get_one_or_create(session,
                      model,
                      create_method='',
                      create_method_kwargs=None,
                      **kwargs):
    try:
        return session.query(model).filter_by(**kwargs).one(), True
    except NoResultFound:
        kwargs.update(create_method_kwargs or {})
        created = getattr(model, create_method, model)(**kwargs)
        try:
            session.add(created)
            session.commit()
            return created, False
        except IntegrityError:
            session.rollback()
            return session.query(model).filter_by(**kwargs).one(), True

```
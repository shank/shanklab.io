+++
author = "Shank"
title = "Today I Learned : How to download Elixir docs for offline use"
date = "2019-02-09T10:40:05+05:30"
draft = false
categories = ["today-i-learned", "elixir"]
tags = ["today-i-learned", "elixir"]
slug = "til-download_elixir_docs_offline"
+++

All the docs published on [hexdocs](https://hexdocs.pm/) can be downloaded for offline use.

To download and open the docs in your browser
```shell
mix hex.docs offline elixir
```

If the docs are not already downloaded, this will download the docs first and then open them in your browser.

To download docs for a particular version
```shell
mix hex.docs fetch elixir 1.8.1
```

---
author: Shank
categories:
- linux
- tail
- utils
date: 2015-06-30T10:39:00+05:30
description: ""
draft: false
slug: multitail
tags:
- linux
- tail
- utils
title: Linux Utils - tail - view the tail end of files or pipes
---

`tail` is a command line program for *nix like systems for viewing the _tail_ end of files or pipes.

Tail is very useful for viewing log files, especially when you want to monitor the new logs being generated in real time.

syntax:  
```shell
tail [options] filename
```

Options that I commonly use:

- View the **last n** lines of a file

        tail -n filename

- To monitor the tail end of a file (updates when new lines are added to the file)

        tail -f filename

- Continue to view a log file that get rotated

        tail -F filename

To break out of tail, use `ctrl + c`
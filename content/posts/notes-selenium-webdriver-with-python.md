---
author: Shank
categories:
- python
- selenium
- webdriver
- automation testing
- testing
date: 2015-06-30T10:46:38+05:30
description: ""
draft: true
slug: notes-selenium-webdriver-with-python
tags:
- python
- selenium
- webdriver
- automation testing
- testing
title: Notes - Course - Selenium Webdriver with Python
---


### Selenium Introduction

**Selenium** is a suite of tools

- **Selenium IDE**
- **Selenium RC** (Remote Control) (Selenium 1)
- **Selenium Grid**
- **Selenium Webdriver** (Selenium 2)

Selenium IDE >> *Firefox plugin* to record tests and playback tests
HTML Format (default). Can be exported in other languages

Selenium 2 is an improvement to Selenium 1 (does not need and additional driver)

JDK is needed to run the *selenium server*

```python
from selenium import webdriver

driver = webdriver.Firefox()
driver.get("http://www.google.com")
driver.quit()
```

### Implicit Wait v/s Explicit Wait

- Implicit Wait
    - Webdriver polls the DOM for a certain amount of time when trying to find an element
    - Set for the lifetime of the webdriver object instance
    - `NoSuchElementException`

- Explicit Wait
    - has a timeout
    - `TimeoutException`

***Expected conditions*** are webdriver expressions that you can use to check for specific things

```python
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

fLocator = 'input[name=q]'

try:
    searchField = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, fLocator)))
finally:
    driver.quit()
```

Webdriver will wait for max of 10 seconds

### Locating Elements

- **Xpath**
- **CSS Selector**
- Sizzle

For Xpath checking use Firebug and Firepath

> Refer [Xpath Cheatsheet](#TODO)

Firepath can also be used for CSS selector checking

> Refer [CSS Cheatsheet](#TODO)

#### Dealing with Dynamic Elements

Dynamic Element > an element with an *identifier* that *changes regularly*

- Locate element by part of its identifier that does not change

        //input[contains(@id, 'Form_')]

- Use Xpath to locate an element relative to a static element

#### Comparing Locator Strategies

- CSS Selectors are faster on IE8 and XPath is faster on Chrome and Firefox everything else.
- CSS Selectors are more readable than Xpath Selectors
- IE does not support Xpath natively

### Overview of Python `unittest` module

#### Understanding the `unittest` module

- also known as **PyUnit**
- sharing of setup and shutdown code for tests
- aggregation of tests into a test suite
- assert methods
- reporting for each tests
- unittest `TestCase` class
- test fixtures
- test runner

#### Structure of a Test Case

```python
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

import unittest

class WaitForElements(unittest.TestCase):

    def setup(self):
        self.driver = webdriver.Firefox()
        self.driver.get('http://travelingtony.weebly.com')

    def test_WaitForCheckoutPhotosButton(self):
        bLocator = "//span[.='Checkout my coolest photos']"
        seeButton = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, bLocator)))

    def teardown(self):
        driver.quit()

if __name__ == '__main__':
    unittest.main()
```

`setup` and `teardown` methods are what are called as ***fixtures***

**test-runners**
- default unittest one
- nose
- py.test

#### Assert Methods

```python
    def test_AssertTitle(self):
        self.assertEqual(
            self.driver.title, "Traveling Tony's Photography - Welcome")
```

#### Creating my first Test Suite

```python
import unittest
from testcase1 import TestCase1 # a module having a test case class
from testcase2 import TestCase2 # another module having a test case class

class TestSuite(unittest.TestSuite):

    def suite():
        suite = unittest.TestSuite()
        suite.addTest(TestCase1('test_waitForSearchField'))
        suite.addTest(TestCase1('test_waitForCheckoutPhotosButton'))
        return suite

if __name__ == '__main__':
    unittest.main()
```

### Webdriver API: Finding Element(s) and Applying Basic Actions

#### Methods to find a SINGLE element
**Find Element By** -> **`find_element_by_*` method**
- Xpath -> `find_element_by_xpath(xpath)`

- CSS Selector      -> `find_element_by_css_selector(css_selector)`

- id                    -> `find_element_by_id(id)`

- Class name            -> `find_element_by_class_name(class_name)`

- Name              -> `find_element_by_name(name)`

- Tag name          -> `find_element_by_tag_name(tag_name)`

- Partial Match of its link text    -> `find_element_by_partial_link_text(link_text)`

- Link Text         -> `find_element_by_link_text(link_text)`

#### Methods to find multiple elements

- just replace `element` with `elements` in the above method names

- returns a list of elements

Using lambdas in in explicitl waits

```python
webElement = WebDriverWait(driver, 10).until(
    lambda driver: driver.find_element_by_xpath(xpath))
```

#### Comparing Mehtods of finding element(s)

- finding by **id** or **name** is the **most efficient**
- finding by **id** or **name** is **more readable**
- finding by *xpath or css is slower than using id or name*

#### Clicking and element or sending keys to an element

```python
# to maximize the browser window
driver.maximize_window()
```

```python
# to click and element
element.click()

# to send keys to an element
element.send_keys('the text to be sent')
```

> Avoid using `time.sleep()` in your tests

#### Selecting an option from a dropdown

For dropdown elements, use the `Select` class to create a `Select` object from the element as below  

**Method 1**
```python
dropDownElement = driver.find_element_by_id(dropdown_id)
dropDown = Select(dropDownElement)

# select and option from the dropdown
dropDown.select_by_visible_text(visible_text)
```

**Method 2**

```python
dropDownOption = "select#wsite-com-product-option option[value='2']"
dropDownOptionElement = WebDriverWait(driver, 10).until(
    lambda driver: driver.find_element_by_css_selector(dropDownOption))
dropDownOptionElement.click()
```

#### Getting an Element's Text or Attribute

```python
# get the text
element.text

# get attribute
element.get_attribute('class')
```

### Webdriver API: Switching Focus to a Window, an Alert or a Frame

#### Switching focus to a window

```python
# returns a list of window handles
all_window_handles = driver.window_handles

# switching to a window
for handle in all_window_handles:
    if handle != main_window_handle[0]:
        driver.switch_to_window(handle)
        break

# the other window is the focussed window now and all the driver
# commands would go to that window
```

#### Switching focus to and Alert

```python
alert = driver.switch_to.alert
alert.dismiss()
```

#### Switching focus to a Frame

```python
# locate an iframe by its id using the normal find_element_by_id()
driver.switch_to.frame(iFrameElement)
```

### Webdriver API: ActionChains

#### ActionChains

Some actions that can be automated using `ActionChains`

- `move_to_element(to_element)`

- `click(on_element=None)`

- `click_and_hold(on_element=None)`

- `double_click(on_element=None)`

- `send_keys_to_element(element, *keys_to_send)`

- `key_down(value, element=None)`

- `key_up(value, element=None)`

- `drag_and_drop(source, target)`

- `release(on_element=None)`

***Usage***

```python
# chain pattern
ActionChains(driver).move_to_element(menu).click(hidden_submenu).perform()

# queing
actions = ActionChains(driver)
actions.move_to_element(menu)
actions.click(hidden_submenu)
actions.perform()
```

#### Moving focus to and element and clicking a submenu

```python
from selenium.webdriver.common.action_chains import ActionChains

# expected conditions visibility of element
WebDriverWait(driver, 10).until(
    EC.visibility_of_element_located((By.XPATH, elemLocatorXpath)))
```

> ActionChains do not always behave as expected, so be careful

#### Sending keys to an element

```python
# Option 1
actions = ActionChains(driver)
actions.send_keys_to_element(searchFieldElement, "Leatherback")
actions.send_keys_to_element(searchFieldElement, Keys.ENTER)
actions.perform()

# Option 2
actions = ActionChains(driver)
actions.click(searchFieldElement)
actions.send_keys("Leatherback")
actions.send_keys(Keys.ENTER)
actions.perform()
```

### Organising and Refactoring your Code

- Write wrapper functions for common tasks like moving to an element, clicking, entering text etc.

- Write a `BaseTestCase` that extracts the setup and teardown functions for opening and exiting the browser respectively

### Design Patterns: Page Objects

#### Explaining Page Objects

#### Implementing Page Objects in your Tests

```python
# basepage.py

from abc import abstractmethod

class BasePage(object):

    def __init__(self, driver):
        self.driver = driver
        self._verify_page()

    @abstractmethod
    def _verify_page(self):
        """This method verifies that we are on the correct page"""

    # other wrapper methods for common browser actions
    # wait_for_element_visibility()
    # click_element()
    # send_keys()

class IncorrectPageException(Exception):
    """This exception should be thrown when trying to instantiate wrong page"""
```

```python
# contactpage.py

from basepage import BasePage

def ContactPage(BasePage):

    def __init__(self, driver):
        super(ContactPage, self).__init__(driver)

    def _verify_page(self):
        try:
            self.wait_for_element_visibility(  
                10,  
                'xpath',  
                "//input[contains(@name, 'first')]"
            )
        except:
            raise IncorrectPageException
```

### Using a UI Map in your Tests

#### How to implement a UI Map

- Use a UI Map to keep all the web element locators in one place
- A single place to update locators (less work)
- use dict to implement UI Map

```python
# sample ui map

ContactPageMap = dict(
    FirstNameFieldXpath = "//input[contains(@name, 'first')]",
    EmailFieldXpath = "(//input[contains(@id, 'input')])[3]"
)

FacebookLoginPageMap = dict(
    ...
)
```

> One Page -> One UI Map

&nbsp;

> A sample naming convention for naming elements in a UI Map
> &lt; element name as it appears on ui &gt; + &lt; type of field &gt; + &lt; type of locator &gt;
> FirstName + Field + Xpath -> FirstNameFieldXpath

### Best Practices for Writing Solid Selenium Tests

#### Write Independent Tests

> Its not always easy to write independent tests but your tests should always be implemented  in a way that gives you visibility as to why a particular test is failing

#### Wrap Selenium Calls

#### Hide Selenium calls from your tests

- There should be no reference to selenium in your tests'source code
- Your tests should only call the methods implemented in your page objects

#### Use Tags

#### Document your Tests

- Explain the goal of each test and the steps taken to achieve the goal
- Add a brief page description of each page object and how to access that particular page from the UI

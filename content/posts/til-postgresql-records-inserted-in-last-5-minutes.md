---
author: Shank
categories:
- today-i-learned
- postgresql
date: 2017-10-02T13:34:35+05:30
description: ""
draft: false
slug: til-postgresql-records-inserted-in-last-5-minutes
tags:
- today-i-learned
- postgresql
title: 'Today I Learned : PostgreSQL - obtain records that were inserted in the last
  5 minutes'
---

```sql
select * from my_table where inserted_at > now() - interval '5 minute'
```
---
author: Shank
categories:
- go
- golang
- gitlab
- cli
- tools
date: 2016-12-09T15:57:00+05:30
description: ""
draft: false
slug: gitlab-cli-interact-with-gitlab-from-the-command-line-currently-focusing-on-issues
tags:
- go
- golang
- gitlab
- cli
- tools
title: 'gitlab-cli : Interact with Gitlab from the command line, currently focusing
  on issues'
---

[gitlab-cli](https://gitlab.com/shank/gitlab-cli) a cli tool that I have written for interacting with Gitlab. Currently the tool focuses on working with issues for a project. It is written in [Go](https://golang.org) and works cross-platform (works with Windows, Linux and OS X).

Refer to the project [README](https://gitlab.com/shank/gitlab-cli#how-do-i-install-it) page for installation instructions.

Here is what the command help says:

<pre>⏚ [shank:~] % <b>gitlab</b>

Usage: gitlab [OPTIONS] COMMAND [arg...]

Gitlab-CLI

Options:
  -v, --version    Show the version and exit

Commands:
  members      Work with project members
  issues       Work with project issues
  labels       Work with labels in this project

Run 'gitlab COMMAND --help' for more information on a command.
</pre>

> Whenever you run the `gitlab` command, it tries to find the Gitlab.com repository with which it has to interact. The tool does this by reading the remote-url for the remote titled `origin` in the current folder. Alternatively it supports the `--pid` flag for specifying the project in the form of `<namespace>/<repositor-name>` from any folder.

All the following examples are run from a demo project folder, which, on my pc, resides at `/Users/shank/Projects/gitlab-cli-demo`

<pre>⏚ [shank:~/Projects/gitlab-cli-demo] master ± git remote -v
origin	git@gitlab.com:shank/gitlab-cli-demo.git (fetch)
origin	git@gitlab.com:shank/gitlab-cli-demo.git (push)
</pre>

The remote titled `origin` for my repository points to `git@gitlab.com:shank/gitlab-cli-demo.git`. So `gitlab` would identify my project id as `shank/gitlab-cli-demo`.

Lets try some commands.

***So, what can I do with issues?***

<pre>⏚ [shank:~/Projects/gitlab-cli-demo] master 2 ± gitlab <b>issues --help</b>

Usage: gitlab issues COMMAND [arg...]

Work with project issues

Commands:
  list         list the project issues
  create       Create issue
  delete       Delete issues
  close        Close Issues
  reopen       Reopen Issues
  assign       Assign Issues
  addlabel     Add label to Issues

Run 'gitlab issues COMMAND --help' for more information on a command.
</pre>

Lets see how to create an issue.

<pre>⏚ [shank:~/Projects/gitlab-cli-demo] master 2 ± gitlab <b>issues create --help</b>

Usage: gitlab issues create [--pid] (--title | TITLE) [--assignee]

Create issue

Arguments:
  TITLE=""     title of the issue

Options:
  -p, --pid=""        <namespace>/<project_name> for the project with which to work. Default value is extracted from the url for remote 'origin'
  -t, --title=""      title of the issue
  -a, --assignee=""   username of the dev to whom the issue is to be assigned
</pre>

Lets go ahead and create some issues.

<pre>⏚ [shank:~/Projects/gitlab-cli-demo] master ± <b>gitlab issues create "first issue"</b>
2016/12/09 19:35:49 pid: shank/gitlab-cli-demo
Issue created: 2
⏚ [shank:~/Projects/gitlab-cli-demo] master ± <b>gitlab issues create "second issue"</b>
2016/12/09 19:36:57 pid: shank/gitlab-cli-demo
Issue created: 3
</pre>

Let's list the current issues for this project.

<pre>⏚ [shank:~/Projects/gitlab-cli-demo] master ± gitlab <b>issues list</b>
2016/12/09 19:37:17 pid: shank/gitlab-cli-demo
    #ID	#IID	TITLE       	STATUS	ASSIGNED TO	LABELS
3706109	   3	second issue	opened
3706097	   2	first issue 	opened
</pre>
*Close issue 3*.
<pre>⏚ [shank:~/Projects/gitlab-cli-demo] master 2 ± gitlab <b>issues close 3</b>
2016/12/09 19:38:04 pid: shank/gitlab-cli-demo
Issue #3706109 closeed
</pre>

List the issues again.

<pre>⏚ [shank:~/Projects/gitlab-cli-demo] master ± gitlab issues list
2016/12/09 19:38:45 pid: shank/gitlab-cli-demo
    #ID	#IID	TITLE       	STATUS	ASSIGNED TO	LABELS
3706109	   3	second issue	closed
3706097	   2	first issue 	opened
</pre>

The status for issue 3 is updated.
But I wish to see only the *issues that are currently open*.

<pre>⏚ [shank:~/Projects/gitlab-cli-demo] master ± gitlab <b>issues list -o</b>
2016/12/09 19:43:31 pid: shank/gitlab-cli-demo
    #ID	#IID	TITLE      	STATUS	ASSIGNED TO	LABELS
3706097	   2	first issue	opened
</pre>

List the *members* for this project

<pre>⏚ [shank:~/Projects/gitlab-cli-demo] master ± gitlab <b>members list</b>
2016/12/09 19:43:52 pid: shank/gitlab-cli-demo
    #	USERNAME	NAME
18022	shank   	Shashank Sharma
</pre>

*Assign* this issue to *shank*

<pre>⏚ [shank:~/Projects/gitlab-cli-demo] master 2 ± gitlab <b>issues assign shank 2</b>
2016/12/09 19:44:49 pid: shank/gitlab-cli-demo
Issue #2 assigned to shank
</pre>

List the issues again.

<pre>⏚ [shank:~/Projects/gitlab-cli-demo] master ± gitlab issues list
2016/12/09 19:45:33 pid: shank/gitlab-cli-demo
    #ID	#IID	TITLE       	STATUS	ASSIGNED TO    	LABELS
3706109	   3	second issue	closed
3706097	   2	first issue 	opened	Shashank Sharma
</pre>

The "Assigned To" column has been updated correctly.

This is just a glimpse of what this tool is currently capable of. It is very much a *work-in-progress*. Please let me know which features should I add to it next by filing an *issue* on the project [issues page](https://gitlab.com/shank/gitlab-cli/issues).

Head over to the [project](http://gitlab.com/shank/gitlab-cli) and give it a go!

BTW, here is the screenshot of the issues page after running the above command :)
![issues](/images/2016/12/Issues-1.jpg)

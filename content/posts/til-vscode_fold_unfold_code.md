---
author: Shank
title: "Today I Learned : VSCode - How to fold / unfold code blocks?"
date: 2019-02-11T14:45:06+05:30
draft: false
categories:
- today-i-learned
- vscode
tags:
- today-i-learned
- vscode
slug: til-vscode_fold_unfol_code
---

A picture is worth a thousand words...

### Fold

![fold](/images/2019/til_vscode_fold_unfold/fold.png)

### Unfold

![unfold](/images/2019/til_vscode_fold_unfold/unfold.png)

Source: [Stackoverflow](https://stackoverflow.com/a/53993043/947472)
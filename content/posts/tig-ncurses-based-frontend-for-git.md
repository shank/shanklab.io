---
author: Shank
categories:
- notes
- git
- tig
date: 2015-09-18T18:52:17+05:30
description: ""
draft: false
slug: tig-ncurses-based-frontend-for-git
tags:
- notes
- git
- tig
title: tig - Text-mode interface for git
---

![Tig](/images/2015/09/tig2-1.png)

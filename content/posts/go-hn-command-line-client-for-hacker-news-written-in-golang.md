---
author: Shank
categories:
- golang
- hackernews
- go-hn
date: 2016-04-03T19:55:01+05:30
description: ""
draft: false
slug: go-hn-command-line-client-for-hacker-news-written-in-golang
tags:
- golang
- hackernews
- go-hn
title: 'go-hn: command line client for hacker news, written in golang'
---

So, I was in need of a command line client for [Hacker News](http://news.ycombinator.com) and have been playing with [Golang](http://golang.org) for some time.

This weekend, I finally got some spare time and I hacked together a command line client for HackerNews in Golang.

The project lives at [gitlab](https://gitlab.com/shank/go-hn).

Its a quick hack and is in no way polished. Currently supports logging in, upvoting, opening an item, navigating to next/prev page.

![go-hn in action](/images/2016/04/go-hn.png)

Will work on it, as and when I get some spare time.

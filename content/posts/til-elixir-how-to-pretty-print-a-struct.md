---
author: Shank
categories:
- elixir
- today-i-learned
date: 2017-09-23T17:46:42+05:30
description: ""
draft: false
slug: til-elixir-how-to-pretty-print-a-struct
tags:
- elixir
- today-i-learned
title: 'Today I Learned : Elixir - How to pretty print a struct'
---

Use the `pretty` option with `inspect()` as below.

```elixir
inspect(data, pretty: true)
```